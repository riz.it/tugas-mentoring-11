<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Haji extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/Api_haji', 'haji');
    }

    public function list_get()
    {
        $get = $this->haji->getHaji();

        if (is_array($get) && $get !=NULL) {

            $this->response([
                'status'=>$this->ok,
                'message'=>$get['message'],
                'data'=>$get['data']],REST_Controller::HTTP_OK);

        } else {
            $this->response([
                'status'=>$this->error,
                'message'=>$get['message'],
                'data'=>NULL], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detail_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if ($data['id_paket']!=NULL || $data['id_paket']!="") {

            $result = $this->haji->detail($data['id_paket']);

            if ($result['status']!="failed") {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']],REST_Controller::HTTP_OK);

            } else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>NULL],REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

            }

        } else {
            $this->response([
                'status'=>$this->bad,
                'message'=>'Parameter data tidak boleh kosong',
                'data'=>NULL], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function registrasi_post()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents("php://input")), true);

                if (!empty($data)) {

                $result = $this->haji->registrasi($data);

                if ($result['status']!='failed') {

                    $this->response([
                        'status'=>$this->ok,
                        'message'=>$result['message'],
                        'data'=>$result['data']], REST_Controller::HTTP_OK);
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>$result['message'],
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }
            }else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>'Data customer harap diisi',
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

            }else {

                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function paketCustomer_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['id_customer']!='' || $data['id_customer']!=NULL) {

                    $result = $this->haji->getPesanan($data);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function detailPaket_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['id_pesanan']!='' || $data['id_pesanan']!=NULL) {

                    $result = $this->haji->detailPesanan($data['id_pesanan']);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function editJamaah_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['id_jamaah']!='' || $data['id_jamaah']!=NULL) {

                    $result = $this->haji->editJamaah($data['id_jamaah']);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function detailTabah_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
        $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['id_jamaah']!='' || $data['id_jamaah']!=NULL) {

                    $result = $this->haji->detailTabah($data['id_jamaah']);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function pendaftarTabah_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
        $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['id_customer']!='' || $data['id_customer']!=NULL) {

                    $result = $this->haji->listPendaftarTabah($data['id_customer']);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function updateJamaah_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $foto_profil = '';

                if ($this->input->post('id_jamaah')!="") {

                    $data_lama = $this->haji->editJamaah($this->input->post('id_jamaah'));

                    if ($this->input->post('ubah_profil')!=NULL && $this->input->post('ubah_profil')=='1') {

                        if ($data_lama['data']['foto_profil']!=NULL || $data_lama['data']['foto_profil']!="") {

                            if (file_exists('./'.$data_lama['data']['foto_profil'])) {
                                unlink('./'.$data_lama['data']['foto_profil']);
                            }
                        }

                        if (isset($_FILES['foto_profil']['name'])) {
                            list($width, $height) = getimagesize($_FILES['foto_profil']['tmp_name']);
                            $config['upload_path'] = 'files/pemesanan_haji/profil/'; //path folder file upload
                            $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                            $config['max_size'] = '2000';
                            $config['file_name'] = "profil_" . date('ymdhis'); //enkripsi file name upload
                            $this->load->library('upload');
                            $this->upload->initialize($config);
                            if ($this->upload->do_upload('foto_profil')) {
                                $file_foto = $this->upload->data();
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = './files/pemesanan_haji/profil/' . $file_foto['file_name'];
                                $config['create_thumb'] = FALSE;
                                $config['maintain_ratio'] = TRUE;
                                $config['quality'] = '70%';
                                $config['width'] = round($width / 2);
                                $config['height'] = round($height / 2);
                                $config['new_image'] = './files/pemesanan_haji/profil/' . $file_foto['file_name'];
                                $this->load->library('image_lib');
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $nama_foto = 'files/pemesanan_haji/profil/' . $file_foto['file_name'];
                                $foto_profil = $nama_foto;
                            }
                        }

                    }else {
                        $foto_profil =$data_lama['data']['foto_profil'];
                    }

                    $data['nama_lengkap'] = $this->input->post('nama_lengkap');
                    $data['jenis_identitas'] = $this->input->post('jenis_identitas');
                    $data['nomor_identitas'] = $this->input->post('nomor_identitas');
                    $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
                    $data['tempat_lahir'] = $this->input->post('tempat_lahir');
                    $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
                    $data['status_kawin'] = $this->input->post('status_kawin');
                    $data['pendidikan'] = $this->input->post('pendidikan');
                    $data['pekerjaan'] = $this->input->post('pekerjaan');
                    $data['id_provinsi'] = $this->input->post('id_provinsi');
                    $data['id_kabupaten'] = $this->input->post('id_kabupaten');
                    $data['id_kecamatan'] = $this->input->post('id_kecamatan');
                    $data['alamat_ktp'] = $this->input->post('alamat_ktp');
                    $data['alamat_domisili'] = $this->input->post('alamat_domisili');
                    $data['golongan_darah'] = $this->input->post('golongan_darah');
                    $data['nama_ayah'] = $this->input->post('nama_ayah');
                    $data['nama_ibu'] = $this->input->post('nama_ibu');
                    $data['email'] = $this->input->post('email');
                    $data['nomor_hp'] = $this->input->post('nomor_hp');
                    $data['nomor_hp_cadangan'] = $this->input->post('nomor_hp_cadangan');
                    $data['nama_ahli_waris'] = $this->input->post('nama_ahli_waris');
                    $data['nomor_hp_ahli_waris'] = $this->input->post('nomor_hp_ahli_waris');
                    $data['alamat_ahli_waris'] = $this->input->post('alamat_ahli_waris');
                    $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');
                    $data['id_referensi_pegawai'] = $this->input->post('id_referensi_pegawai');
                    $data['foto_profil'] = $foto_profil;

                    $result = $this->haji->updateJamaah($data, $this->input->post('id_jamaah'));

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }

                } else {
                    $this->response([
                        'status' => $this->unauthorized,
                        'message' => 'Unathorized/Invalid Token',
                        'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
                }

            } else {
                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter kosong, harap lengkapi data anda',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function daftarTabah_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $date = date("Y-m-d H:i:s");

                $cekTabungan = $this->haji->cekTabunganJamaah($this->input->post('id_jamaah'));

                if ($cekTabungan!=0) {
                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data jamaah sudah terdaftar dalam aplikasi',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }else {

                    $this->db->trans_begin();

                    $deadline_setoran = date('Y-m-d H:i:s', strtotime('+30 days', strtotime($date)));

                    $tabah['id_tabungan'] = getUUID();
                    $tabah['fk_jamaah'] = $this->input->post('id_jamaah');
                    $tabah['kode_reg'] = auto_code('TABAH-'.date('ym'),"");
                    $tabah['setoran_awal'] = str_replace(".", "", $this->input->post('setoran_awal'));
                    $tabah['sisa_setoran_awal'] = str_replace(".", "", $this->input->post('setoran_awal'));
                    $tabah['target_tabungan'] = str_replace(".", "", $this->input->post('target_tabungan'))-$tabah['setoran_awal'];
                    $tabah['sisa_target_tabungan'] = str_replace(".", "", $this->input->post('target_tabungan'))-$tabah['setoran_awal'];
                    $tabah['setoran_perbulan'] = str_replace(".", "", $this->input->post('setoran_perbulan'));
                    $tabah['deadline_setoran_awal'] = $deadline_setoran;
                    $tabah['target_lunas'] = $this->input->post('target_lunas');
                    $tabah['status_tabungan'] = 'MENDAFTAR';
                    $tabah['created_by'] = $this->input->post("id_user");

                    $jamaah['jenis_dokumen_pendukung'] = $this->input->post('dokumen_pendukung');
                    $jamaah['nomor_porsi'] = $this->input->post('nomor_porsi');

                    if (isset($_FILES['scan_kk']) && $_FILES['scan_kk']['error'] === 0) {
                        $up1 = $this->_uploadFile('scan_kk','tabah/kk','KK');
                        if ($up1['success']) {
                            $jamaah['scan_kk'] = $up1['file_name'];
                        }
                    }

                    if (isset($_FILES['scan_surat_kesehatan']) && $_FILES['scan_surat_kesehatan']['error'] === 0) {
                        $up2 = $this->_uploadFile('scan_surat_kesehatan','tabah/surat_kesehatan', 'SURAT_KESEHATAN');
                        if ($up2['success']) {
                            $jamaah['scan_surat_kesehatan'] = $up2['file_name'];
                        }
                    }

                    if (isset($_FILES['scan_dokumen_pendukung']) && $_FILES['scan_dokumen_pendukung']['error'] === 0) {
                        $up3 = $this->_uploadFile('scan_dokumen_pendukung','tabah/dokumen_pendukung', str_replace(" ", "_", $this->input->post('dokumen_pendukung')));
                        if ($up3['success']) {
                            $jamaah['url_dokumen_pendukung'] = $up3['file_name'];
                        }
                    }

                    if (isset($_FILES['scan_bpih']) && $_FILES['scan_bpih']['error'] === 0) {
                        $up4 = $this->_uploadFile('scan_bpih','tabah/bpih', 'BPIH');
                        if ($up4['success']) {
                            $jamaah['url_nomor_porsi'] = $up4['file_name'];
                        }
                    }

                    $this->db->where("REPLACE(id_jamaah,'-','')", $this->input->post('id_jamaah'));
                    $this->db->update("hj_pemesanan_jamaah", $jamaah);

                    $this->db->insert("haji_tabungan", $tabah);

                    if ($this->db->trans_status()===false) {
                        $this->db->trans_rollback();
                        $this->response([
                            'status'=>$this->error,
                            'message'=>'Pendaftaran Tabungan Pelunasan Haji Gagal.',
                            'data'=>'0'], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }else {
                        $this->db->trans_commit();
                        $this->response([
                        'status'=>$this->ok,
                        'message'=>'Pendaftaran tabungan pelunasan paket haji berhasil',
                        'data'=>'1'], REST_Controller::HTTP_OK);
                    }
                    $this->db->trans_complete();

                }

            } else {
                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {
            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function pembayaran_post()
    {
        $url_bukti = '';
        if (isset($_FILES['url_bukti']['name'])) {
            list($width, $height) = getimagesize($_FILES['url_bukti']['tmp_name']);
            $config['upload_path'] = 'files/pembayaran/'; //path folder file upload
            $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
            $config['max_size'] = '2000';
            $config['file_name'] = "konfirmasi_pembayaran_" . date('ymdhis'); //enkripsi file name upload
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('url_bukti')) {
                $file_foto = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '50%';
                $config['width'] = round($width / 2);
                $config['height'] = round($height / 2);
                $config['new_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $nama_foto = 'files/pembayaran/' . $file_foto['file_name'];
                $url_bukti = $nama_foto;
            }
        }

        $data['url_bukti'] = $url_bukti;
        $data['nomor_invoice'] = $this->input->post('nomor_invoice');
        $data['jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
        $data['tanggal_transfer'] = $this->input->post('tanggal_transfer');
        $data['bank_tujuan'] = $this->input->post('bank_tujuan');
        $data['jumlah_dana'] = $this->input->post('jumlah_dana');
        $data['nama_pengirim'] = $this->input->post('nama_pengirim');
        $data['email_jamaah'] = $this->input->post('email_jamaah');
        $data['no_telpon_jamaah'] = $this->input->post('no_telpon_jamaah');
        $data['status'] = '01';

        $result = $this->tabrur->konfirmasiPembayaran($data);

        if ($result['status']!='failed') {

            $this->response([
                'status'=>$this->ok,
                'message'=>$result['message'],
                'data'=>$result['data']], REST_Controller::HTTP_OK);

        }else {
            $this->response([
            'status'=>$this->bad,
            'message'=>$result['message'],
            'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function cekPembayaran_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        if ($data['kode_pembayaran']!='' || $data['kode_pembayaran']!=NULL) {

            $result = $this->tabrur->cekPembayaran($data);

            if ($result!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    function jajalan()
    {
        return 'sukses';
    }

    function _uploadFile($nama_file, $path_folder, $prefix)
    {
        $response['success'] = false;
        $response['file_name'] ='';
        $nama_foto = "";
        if (isset($_FILES[$nama_file]['name'])) {
            list($width, $height) = getimagesize($_FILES[$nama_file]['tmp_name']);
            $config['upload_path']='files/'.$path_folder; //path folder file upload
            $config['allowed_types']='gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
            $config['max_size'] = '2000';
            $config['file_name'] = $prefix.'_'.auto_code(date('ymd')); //enkripsi file name upload
            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload($nama_file)) {
                $file_foto = $this->upload->data();
                $nama_foto='files/'.$path_folder.'/'.$file_foto['file_name'];
                $response['success'] = true;
                $response['file_name'] =$nama_foto;
            }
        }

        return $response;
    }




}
