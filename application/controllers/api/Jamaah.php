<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';
require APPPATH.'/libraries/mpdf/autoload.php';

use Restserver\Libraries\REST_Controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class Jamaah extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_jamaah', 'jamaah');
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    public function jamaah_post() {

        $data =json_decode(trim(file_get_contents('php://input')), true);
        if (!empty($data)) {
            $get = $this->jamaah->getJamaahByPemesanan($data['id_pemesanan']);
            if (is_array($get) && $get != null) {
                if ($get['status'] == 'ok') {
                    $result = $get['data'];

                    $this->response([
                        'status' => $this->ok,
                        'data' => $result
                            ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => $get['message']
                            ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => 'Data tidak ditemukan'
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        }else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function edit_post() {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=false && property_exists($decoded_token, "id_user")) {

                $data =json_decode(trim(file_get_contents('php://input')), true);
                if (!empty($data)) {
                    $get = $this->jamaah->edit($data['id_jamaah']);
                    if (is_array($get) && $get != null) {
                        if ($get['status'] == 'ok') {
                            $result = $get['data'];

                            $this->response([
                                'status' => $this->ok,
                                'message'=>'Data jamaah ditemukan',
                                'data' => $result
                                    ], REST_Controller::HTTP_OK);
                        } else {
                            $this->response([
                                'status' => $this->notfound,
                                'message' => $get['message'],
                                'data'=>''], REST_Controller::HTTP_NOT_FOUND);
                        }
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'message' => 'Data tidak ditemukan',
                            'data'=>''], REST_Controller::HTTP_NOT_FOUND);
                    }
                }else {
                    $this->response([
                        'status' => $this->notfound,
                        'message' => 'Data tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_NOT_FOUND);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function updated_post()
    {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= false && property_exists($decoded_token, "id_user")) {

                if (!empty($this->input->post('id_jamaah'))) {
                    $data_lama = $this->jamaah->edit($this->input->post('id_jamaah'));

                    if ($this->input->post('ubah_foto')!=NULL && $this->input->post('ubah_foto')=='1') {

                        if ($data_lama['data']['foto_profil']!='' || $data_lama['data']['foto_profil']!=NULL) {
                            if (file_exists('./'.$data_lama['data']['foto_profil'])) {
                                unlink('./'.$data_lama['data']['foto_profil']);
                            }
                        }


                        if (isset($_FILES['foto_profil']['name'])) {
                            $config['upload_path']='files/jamaah/'; //path folder file upload
                            $config['allowed_types']='gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                            $config['max_size'] = '3000';
                            $config['file_name'] = 'jamaah_'.date('ymdhis'); //enkripsi file name upload
                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('foto_profil')) {
                                $file_foto = $this->upload->data();
                                $config['image_library']='gd2';
                                $config['source_image']='./files/jamaah/'.$file_foto['file_name'];
                                $config['create_thumb']= FALSE;
                                $config['maintain_ratio']= TRUE;
                                $config['quality']= '50%';
                                $config['new_image']= './files/jamaah/'.$file_foto['file_name'];
                                $this->load->library('image_lib');
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $nama_foto='files/jamaah/'.$file_foto['file_name'];
                                $update['foto_profil'] = $nama_foto;
                            }
                        }
                    }else {
                        $update['foto_profil'] = $data_lama['data']['foto_profil'];
                    }

                    if ($data_lama['data']['url_invoice']!='' || $data_lama['data']['url_invoice']!=NULL) {
                        if (file_exists('./'.$data_lama['data']['url_invoice'])) {
                            unlink('./'.$data_lama['data']['url_invoice']);
                        }
                    }


                    $update['nama_lengkap'] = $this->input->post('nama_lengkap');
                    $update['jenis_identitas'] = $this->input->post('jenis_identitas');
                    $update['nomor_identitas'] = $this->input->post('nomor_identitas');
                    $update['jenis_kelamin'] = $this->input->post('jenis_kelamin');
                    $update['tempat_lahir'] = $this->input->post('tempat_lahir');
                    $update['tanggal_lahir'] = $this->input->post('tanggal_lahir');
                    $update['status_kawin'] = $this->input->post('status_kawin');
                    $update['pendidikan'] = $this->input->post('pendidikan');
                    $update['pekerjaan'] = $this->input->post('pekerjaan');
                    $update['id_provinsi'] = $this->input->post('id_provinsi');
                    $update['id_kota'] = $this->input->post('id_kota');
                    $update['id_kecamatan'] = $this->input->post('id_kecamatan');
                    $update['alamat_ktp'] = $this->input->post('alamat_ktp');
                    $update['nama_ayah'] = $this->input->post('nama_ayah');
                    $update['email'] = $this->input->post('email');
                    $update['nomor_hp'] = $this->input->post('nomor_hp');
                    $update['nomor_hp_cadangan'] = $this->input->post('nomor_hp_cadangan');
                    $update['kewarganegaraan'] = $this->input->post('kewarganegaraan');
                    $update['id_referensi_pegawai'] = $this->input->post('id_referensi_pegawai');
                    $this->db->trans_begin();
                    $this->db->where("REPLACE(id_jamaah,'-','')", str_replace("-", "", $this->input->post('id_jamaah')));
                    $this->db->update("um_pemesanan_jamaah", $update);

                    $this->db->where("id_jamaah", $this->input->post('id_jamaah'));
                    $this->db->delete("um_pemesanan_biaya_tambahan");
                    $biaya_tambahan = count(unserialize($this->input->post('biaya_tambahan')));
                    $data_biaya = unserialize($this->input->post('biaya_tambahan'));
                      for ($i = 0; $i < $biaya_tambahan; $i++) {

                        $td_biaya[$i]['id_biaya'] = getUUID();
                        $td_biaya[$i]['id_jamaah'] = $this->input->post('id_jamaah');
                        $td_biaya[$i]['fk_biaya_tambahan'] = $data_biaya[$i]['fk_biaya_tambahan'];
                        $td_biaya[$i]['mata_uang'] = $data_biaya[$i]['mata_uang'];
                        $td_biaya[$i]['jumlah'] = $data_biaya[$i]['jumlah'];
                        $this->db->insert("um_pemesanan_biaya_tambahan", $td_biaya[$i]);
                      }

                    if ($this->db->trans_status()===false) {
                        $this->db->trans_rollback();
                        $this->response([
                            'status' => $this->bad,
                            'message' => $this->db->error()
                                ], REST_Controller::HTTP_BAD_REQUEST);
                        return;
                    }else {
                        $this->db->trans_commit();

                        $data_invoice = $this->jamaah->get_detail(str_replace("-", "", $this->input->post('id_jamaah')));
                        $invoices['jamaah'] = $data_invoice['data'];

                        $invoice_file = auto_code('RSO'.date('ymd'),'');
                        $mpdf = new \Mpdf\Mpdf(['format' => 'A4']);
                        $mpdf->useSubstitutions = false;
                        $mpdf->simpleTables = true;
                        $mpdf->AddPage('P'); // Adds a new page in Landscape orientationw
                        $mpdf->WriteHTML($this->load->view('konfirmasi_paket_umroh',$invoices, true));
                        $mpdf->Output(FCPATH.'files/invoice/'.$invoice_file.'.pdf','F');
                        $this->db->where("REPLACE(id_jamaah,'-','')", str_replace("-", "", $this->input->post('id_jamaah')));
                        $this->db->update('um_pemesanan_jamaah', array('url_invoice'=>'files/invoice/'.$invoice_file.'.pdf'));

                        $mail = new PHPMailer();

                        $invoices['link'] = base_url().'files/invoice/'.$invoice_file.'.pdf';

                        $html = $this->load->view('kirim_konfirmasi_umroh',$invoices, true);

                        $mail->IsHTML(true);    // set email format to HTML
                        $mail->IsSMTP();   // we are going to use SMTP
                        $mail->SMTPAuth   = true; // enabled SMTP authentication
                        $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
                        $mail->SMTPAutoTLS = false;
                        // $mail->SMTPDebug  = 2;
                        $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
                        $mail->Port       = 465;   // SMTP port to connect to GMail
                        $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
                        $mail->Password   = "www.rosanatourtravel.com1";            // password GMail
                        $mail->SetFrom("cyber.rosana@gmail.com", 'Penting !');  //Siapa yg mengirim email
                        $mail->Subject    = 'Invoice pesanan anda';
                        $mail->Body       = $html;
                        $mail->AddAddress($update['email']);
                        $mail->WordWrap = 50;
                        $mail->Priority = 1;
                        $mail->AddCustomHeader("X-MSMail-Priority: High");
                        if(!$mail->Send()) {
                          echo $mail->ErrorInfo;
                        }

                        $mail->ClearAddresses();

                        $this->response([
                            'status' => $this->ok,
                            'message' => 'Data customer berhasil diupdate'
                                ], REST_Controller::HTTP_OK);
                        return;
                    }
                    $this->db->trans_complete();
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function maskapai_get() {
        $get = $this->master->maskapai_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function bandara_get() {
        $get = $this->master->bandara_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function biaya_tambahan_get() {
        $get = $this->master->biaya_tambahan();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


     // ini contoh
    public function get_province() {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->provinces_data();
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    //ini contoh
    public function get_regency() {
        $headers = $this->input->request_headers();
        $query_url = $this->input->get();

        // query parameter
        $province_id = null; // if null, get all regencies
        if ($query_url != false && array_key_exists('province_id', $query_url)) {
            $province_id = $query_url['province_id'];
        }

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->regencies_data($province_id);
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    //ini contoh
    public function get_district() {
        $headers = $this->input->request_headers();
        $query_url = $this->input->get();

        // query parameter
        $regency_id = null; // if null, get all regencies
        if ($query_url != false && array_key_exists('regency_id', $query_url)) {
            $regency_id = $query_url['regency_id'];
        }

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->districts_data($regency_id);
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
