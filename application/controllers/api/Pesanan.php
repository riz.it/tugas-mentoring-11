<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';
require APPPATH.'/libraries/mpdf/autoload.php';

use Restserver\Libraries\REST_Controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class Pesanan extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';


    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_pemesanan', 'pemesanan');
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    public function konfirmasi_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
        $decoded_token = AUTHORIZATION::validateToken($token);

        if ($decoded_token!= false && property_exists($decoded_token, "id_user")) {

          $data =json_decode(trim(file_get_contents('php://input')), true);
          if (is_array($data)) {
              $this->db->trans_begin();

              $dtjamaah = count($data['jamaah']);
              $dt_harga_total=0;
              for ($i = 0; $i < $dtjamaah; $i++) {
                $dt_harga_total += $data['jamaah'][$i]['harga'];
              }

                $startDate = time();
                $expired= date('Y-m-d H:i:s', strtotime('+3 day', $startDate));

          $register['id_pemesanan'] = getUUID();
          $register['id_customer'] = $data['identitas']['id_customer'];
          $register['created_by'] = $data['identitas']['id_user'];
          $register['fk_program_umrah'] = $data['umroh']['fk_program_umrah'];
          $register['tanggal_jatuh_tempo_dp'] = $expired;
          $register['metode_pembayaran'] = $data['umroh']['metode_pembayaran'];
          $register['bank'] = $data['umroh']['bank'];
          $register['id_cabang'] = $data['umroh']['id_cabang'];
          $register['total_pembayaran'] = $dt_harga_total;
          $register['status_pembayaran'] = 'PENDING';
          $register['kode_pembayaran'] = auto_code('INV-'.date('ym'),'');
          $register['prefix'] = $register['kode_pembayaran'];

          $this->db->set($register);
          $this->db->insert('um_pemesanan', $register);

          $jamaah = count($data['jamaah']);
          for ($i = 0; $i < $jamaah; $i++) {
            $dt_jamaah[$i]['id_jamaah'] = getUUID();
            $dt_jamaah[$i]['fk_id_pemesanan'] = $register['id_pemesanan'];
            $dt_jamaah[$i]['nama_lengkap'] = $data['jamaah'][$i]['nama_lengkap'];
            $dt_jamaah[$i]['kategori'] = $data['jamaah'][$i]['kategori'];
            $dt_jamaah[$i]['pilihan_kamar'] = $data['jamaah'][$i]['jenis_kamar'];
            $dt_jamaah[$i]['harga'] = $data['jamaah'][$i]['harga'];
            $dt_jamaah[$i]['created_by'] = $data['identitas']['id_user'];
            $this->db->insert('um_pemesanan_jamaah',$dt_jamaah[$i]);
          }

          $program_umroh = $this->db->query("SELECT * FROM um_kelas_program where REPLACE(id_kelas_program,'-','')=?", array(str_replace("-", "", $data['umroh']['fk_program_umrah'])))->row_array();

          $sisa_seat = $program_umroh['pagu']-$jamaah;
          $this->db->where("REPLACE(id_kelas_program,'-','')", str_replace("-", "", $data['umroh']['fk_program_umrah']));
          $this->db->update("um_kelas_program", array("pagu"=>$sisa_seat));

          $data_invoice = $this->pemesanan->get_detail($register['id_pemesanan']);
          $invoices['pemesanan'] = $data_invoice['data'];

          $invoice_file = auto_code('RSO-'.date('ymd'),'');
          $mpdf = new \Mpdf\Mpdf(['format' => 'A4']);
          $mpdf->useSubstitutions = false;
          $mpdf->simpleTables = true;
          $mpdf->AddPage('P'); // Adds a new page in Landscape orientationw
          $mpdf->WriteHTML($this->load->view('konfirmasi_pemesanan',$invoices, true));
          $mpdf->Output(FCPATH.'files/invoice/'.$invoice_file.'.pdf','F');
          $this->db->where("id_pemesanan", $register['id_pemesanan']);
          $this->db->update('um_pemesanan', array('url_invoice'=>'files/invoice/'.$invoice_file.'.pdf'));


          if ($this->db->trans_status()===false) {
              $this->db->rollback();
              $this->response(['status'=>$this->error,'message'=>'Entri data failed','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
          }else {
              $this->db->trans_commit();

              $mail = new PHPMailer();

              $invoices['link'] = '/files/invoice/'.$invoice_file.'.pdf';

              $html = $this->load->view('kirim_invoice',$invoices, true);

              $mail->IsHTML(true);    // set email format to HTML
              $mail->IsSMTP();   // we are going to use SMTP
              $mail->SMTPAuth   = true; // enabled SMTP authentication
              $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
              $mail->SMTPAutoTLS = false;
              // $mail->SMTPDebug  = 2;
              $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
              $mail->Port       = 465;   // SMTP port to connect to GMail
              $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
              $mail->Password   = "www.rosanatourtravel.com1";            // password GMail
              $mail->SetFrom("cyber.rosana@gmail.com", 'Penting !');  //Siapa yg mengirim email
              $mail->Subject    = 'Invoice pesanan anda';
              $mail->Body       = $html;
              $mail->AddAddress($data_invoice['data']['email']);
              $mail->WordWrap = 50;
              $mail->Priority = 1;
              $mail->AddCustomHeader("X-MSMail-Priority: High");
              if(!$mail->Send()) {
                echo $mail->ErrorInfo;
              }

              $mail->ClearAddresses();

          }
          $this->db->trans_complete();
          $this->response(['status'=>$this->ok,'message'=>'Data pemesanan paket umroh berhasil diproses, harap cek email anda untuk mengunduh invoice yang dikirimkan','data'=>'1'], REST_Controller::HTTP_OK);

          }else {
              $this->response(['status'=>$this->error,'message'=>'Entri data failed','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

      }else {

        $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    public function getPesanan_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
        $decoded_token = AUTHORIZATION::validateToken($token);

        if ($decoded_token!= false && property_exists($decoded_token, 'id_user')) {

          $data =json_decode(trim(file_get_contents('php://input')), true);
          if (!empty($data)) {
              $result = $this->pemesanan->pemesanan_customer($data['id_customer']);
              if (!empty($result)) {
                  $this->response(['status'=>$this->ok,'message'=>$result['message'], 'data'=>$result['data']], REST_Controller::HTTP_OK);
              }else {
                  $this->response(['status'=>$this->error,'message'=>'Data customer tidak ditemukan','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
              }
          }else {
              $this->response(['status'=>$this->error,'message'=>'Data customer tidak terdaftar','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

      }else {
        $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    public function detailPesanan_post()
    {

      $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
        $decoded_token = AUTHORIZATION::validateToken($token);

        if ($decoded_token!=false && property_exists($decoded_token, "id_user")) {

          $data =json_decode(trim(file_get_contents('php://input')), true);
          if (!empty($data)) {
            $result = $this->pemesanan->detail_pesanan($data['id_pemesanan']);
            if (!empty($result)) {
                    $this->response(['status'=>$this->ok,'message'=>'Data pemesanan', 'data'=>$result['data']], REST_Controller::HTTP_OK);
                }else {
                    $this->response(['status'=>$this->error,'message'=>'Data pemesanan tidak ditemukan','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                }
          }else {
                $this->response(['status'=>$this->error,'message'=>'Data pesanan tidak terdaftar','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

      }else {
        $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }
}
