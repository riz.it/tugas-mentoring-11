<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "product";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_product');
        $this->cek_hak_akses();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Daftar Produk';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Akun</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('products');
        $crud->setRelation('category_id', 'categories', 'name');
        $crud->columns(['name', 'category_id', 'price', 'stock', 'color', 'is_active']);
        $crud->defaultOrdering('name', 'asc');
        $addFields = ['name', 'category_id', 'price', 'stock', 'color'];
        $editFields = ['name', 'category_id', 'price', 'stock', 'color', 'is_active'];
        $crud->addFields($addFields);
        $crud->editFields($editFields);
        $crud->displayAs(
            [
                'name' => 'Nama Produk',
                'category_id' => 'Kategori',
                'price' => 'Harga',
                'stock' => 'Stok',
                'color' => 'Warna',
                'is_active' => 'Status',
            ]
        );
        $status = array(
            '0' => 'NonAktif',
            '1' => 'Aktif',
        );
        $crud->fieldType('is_active', 'dropdown', $status);
        $crud->callbackColumn('price', function ($value, $row) {
            return 'Rp. ' . number_format($value, 0, ',', '.');
        });

        $crud->callbackColumn('is_active', function ($value, $row) {
            if ($value == '1') {
                return '<span class="badge badge-success">Aktif</span>';
            } else {
                return '<span class="badge badge-danger">NonAktif</span>';
            }
        });

        $crud->setActionButton('Detail', 'fas fa-eye', function ($row) {
            return "javascript:showModalDetail('" . $row->uuid . "')";
        });


        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['uuid'] = generate_unique_uuid('products', 'uuid');
            $stateParameters->data['slug'] = url_title($stateParameters->data['name'], '-', true);
            $stateParameters->data['is_active'] = '0';
            $stateParameters->data['created_at'] = date('Y-m-d H:i:s');
            return $stateParameters;
        });

        $crud->callbackBeforeUpdate(function ($stateParameters) {
            $stateParameters->data['updated_at'] = date('Y-m-d H:i:s');
            $stateParameters->data['slug'] = url_title($stateParameters->data['name'], '-', true);
            return $stateParameters;
        });



        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output, 'product');
    }

    public function getProductByUUID($param)
    {
        $data_produk = $this->M_product->getByUUID($param);
        $created_at = date("d-M-Y", strtotime($data_produk["created_at"]));
        $status_badge = ($data_produk["is_active"] == "1") ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Non-Aktif</span>';


        $modal_body = '
            <h4>' . $data_produk["name"] . '</h4>
            <ul>
                <li>Stok : ' . $data_produk["stock"] . '</li>
                <li>Status : ' . $status_badge . '</li>
                <li>Kategori : ' . $data_produk["category_name"] . '</li>
                <li>Color : ' . $data_produk["color"] . '</li>
                <li>Price : Rp. ' . number_format($data_produk["price"], 0, ",", ".") . '</li>
                <li>Dibuat pada : ' . $created_at . '</li>
            </ul>
        ';
        echo ($modal_body);
    }
}
