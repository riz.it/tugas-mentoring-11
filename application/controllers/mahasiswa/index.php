<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "mahasiswa";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mahasiswa');
        $this->load->model('M_hobi');
        $this->cek_hak_akses();
    }

    public function index()
    {

        $this->data['nama_menu'] = 'Data Mahasiswa';
        $this->data['output'] = (object)[
            'css_files' => [
                base_url('public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
                base_url('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'),
                base_url('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'),
                base_url('public/plugins/select2/css/select2.min.css')
            ],
            'js_files' => [
                base_url('public/plugins/select2/js/select2.full.min.js'),
                base_url('public/plugins/datatables/jquery.dataTables.min.js'),
                base_url('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'),
                base_url('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'),
                base_url('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')
            ]
        ];
        $this->data['hobi'] = $this->M_hobi->getAllHobi();
  
        $this->data['mahasiswa'] = $this->M_mahasiswa->getAllMahasiswa();


        $this->render("index");
    }
    

    public function edit($param)
    {

        $this->data['nama_menu'] = 'Data Mahasiswa';
        $this->data['output'] = (object)[
            'css_files' => [
                base_url('public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
                base_url('public/plugins/select2/css/select2.min.css')
            ],
            'js_files' => [
                base_url('public/plugins/select2/js/select2.full.min.js'),
            ]
        ];
        $this->data['hobi'] = $this->M_hobi->getAllHobi();
  
        $this->data['mahasiswa'] = $this->M_mahasiswa->getMahasiswaById($param);


        $this->render("edit");
    }

    public function save()
    {
        $data = $this->input->post();
        $insert = $this->M_mahasiswa->storeMahasiswa($data);
        if ($insert['status']) {
            $this->session->set_flashdata('success', $insert['message']);
        } else {
            $this->session->set_flashdata('error', $insert['message']);
        }
        redirect(base_url('index.php/mahasiswa/index'));
    }

    public function delete($param)
    {
        $delete = $this->M_mahasiswa->deleteMahasiswa($param);
        if ($delete['status']) {
            $this->session->set_flashdata('success', $delete['message']);
        } else {
            $this->session->set_flashdata('error', $delete['message']);
        }
        redirect(base_url('index.php/mahasiswa/index'));
    }

    public function update($param)
    {
        $data = $this->input->post();
        $insert = $this->M_mahasiswa->updateMahasiswa($data, $param);
        if ($insert['status']) {
            $this->session->set_flashdata('success', $insert['message']);
        } else {
            $this->session->set_flashdata('error', $insert['message']);
        }
        redirect(base_url('index.php/mahasiswa/index'));
    }
}
