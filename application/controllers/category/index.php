<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "category";

    public function __construct()
    {
        parent::__construct();
        $this->cek_hak_akses();
        $this->load->model('M_category');
        $this->load->model('M_product');
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Daftar Kategori';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Akun</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('categories');

        $crud->columns(['name']);

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['uuid'] = generate_unique_uuid('categories', 'uuid');
            return $stateParameters;
        });

        $crud->callbackBeforeDelete(function ($stateParameters) {
            $id = $stateParameters->primaryKeyValue;
            $data = $this->M_product->getByCategory($id);

            if (empty($data)) {
                return $stateParameters;
            } else {
                $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
                return $errorMessage->setMessage("Gagal menghapus data kategori telah digunakan\n");
            }
        });

        $crud->callbackBeforeDeleteMultiple(function ($stateParameters) {
            $id = $stateParameters->primaryKeys;
            foreach ($id as $key) {
                $data = $this->M_product->getByCategory($key);

                if (empty($data)) {
                    return $stateParameters;
                } else {
                    $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
                    return $errorMessage->setMessage("Gagal menghapus data kategori telah digunakan\n");
                }
            }
        });

        $crud->fields(['name']);
        $crud->requiredFields(['name']);
        $crud->defaultOrdering('name', 'asc');

        $crud->displayAs('name', 'Kategori');


        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output, 'category');
        // $this->data['categories'] = $this->M_category->get();
        // $this->render("index");
    }
}
