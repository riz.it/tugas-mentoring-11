<?php $this->load->view('template/template_scripts') ?>
<style>
  .gc-header-tools {
    margin-left: 8px;
    margin-right: 8px;
  }
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
        </div>
      </div>
    </div>
  </section>
  <div class="section-body">

    <div class="card">
      <div class="card-body">
        <?php echo $output->output; ?>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modalDetail" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}

?>
<script>
  function showModalDetail($param) {
    $.ajax({
      type: "GET",
      url: "<?php echo base_url('index.php/product/index/getProductByUUID/') ?>" + $param,
      success: function(response) {
        console.log(response)
        $('.modal-body').html(response);
        $('#modalDetail').modal('show');
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
      }
    })
  }
</script>