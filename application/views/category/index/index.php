<?php $this->load->view('template/template_scripts') ?>
<style>
  .gc-header-tools {
    margin-left: 8px;
    margin-right: 8px;
  }
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
        </div>
      </div>
    </div>
  </section>
  <div class="section-body">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Semua Kategori</h3>
        
      </div>

      <div class="card">
        <div class="card-body">
            <?php echo $output->output; ?>
        </div>
      </div>

    </div>
  </div>
</div>
<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}

?>