<?php $this->load->view('template/template_scripts') ?>
<style>
    .gc-header-tools {
        margin-left: 8px;
        margin-right: 8px;
    }

    .select2-selection__rendered {
        line-height: 28px !important;
    }

    .select2-container .select2-selection--single {
        height: 37px !important;
    }

    .select2-selection__arrow {
        height: 37px !important;
    }

    .select2-selection__choice {
        color: black !important;
    }
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                </div>
            </div>
        </div>
    </section>
    <div class="section-body">
        <form action="<?= base_url() ?>index.php/mahasiswa/index/update/<?= $mahasiswa['id'] ?>" method="POST">
            <div class="card">
                <div class="card-header">
                    <h3>Edit Mahasiswa</h3>
                </div>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="nim">NIM</label>
                                    <input type="number" value="<?= $mahasiswa['nim'] ?>" name="nim" class="form-control" id="nim" placeholder="Masukan NIM">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" value="<?= $mahasiswa['nama'] ?>" name="nama" class="form-control" id="nama" placeholder="Masukan nama">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="jk" class="form-control select2" style="width: 100%;height: 100%">
                                        <option selected="selected" disabled>Pilih</option>
                                        <option <?= ($mahasiswa['jenis_kelamin'] == "L") ? 'selected' : '' ?> value="L">Laki Laki</option>
                                        <option <?= ($mahasiswa['jenis_kelamin'] == "P") ? 'selected' : '' ?> value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" style="height: 38px;" class="form-control" rows="3" placeholder="Masukan alamat"><?= $mahasiswa['alamat']; ?>
                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Hobi</label>
                                    <select name="hobi[]" id="hobi" class="select2" multiple="multiple" data-placeholder="Pilih hobi" style="width: 100%;">
                                        <?php foreach ($hobi as $item) : ?>
                                            <?php
                                            $selected = in_array($item['id'], $mahasiswa['hobi']) ? 'selected' : '';
                                            ?>

                                            <option value="<?= $item['id'] ?>" <?= $selected ?>><?= $item['hobi'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="<?= base_url() ?>index.php/mahasiswa/index" class="btn mx-1 btn-default">Kembali</a>
                    <button type="submit" class="btn mx-1 btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>
<script>
    $(function() {
        $('.select2').select2()


        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'classic'
        })
    });
</script>