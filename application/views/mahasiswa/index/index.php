<?php $this->load->view('template/template_scripts') ?>
<style>
    .gc-header-tools {
        margin-left: 8px;
        margin-right: 8px;
    }

    .select2-selection__rendered {
        line-height: 28px !important;
    }

    .select2-container .select2-selection--single {
        height: 37px !important;
    }

    .select2-selection__arrow {
        height: 37px !important;
    }
    .select2-selection__choice{
        color: black !important;
    }
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                </div>
            </div>
        </div>
    </section>
    <div class="section-body">
        <?php if($this->session->flashdata('success')) : ?>
            <div>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?= $this->session->flashdata('success') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
            </div>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
            <div>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?= $this->session->flashdata('error') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
            </div>
        <?php endif; ?>
      
        <div class="card">
            <div class="card-header">
                <div class="row d-flex justify-content-between">
                    <h3 class="card-title my-auto">Semua Mahasiswa</h3>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                        Tambah Mahasiswa
                    </button>
                </div>

            </div>

            <div class="card-body p-3">
                <table id="mahasiswa" class="table table-striped">
                    <thead>
                        <tr>
                            <th  class="text-center">#</th>
                            <th  class="text-left">NIM</th>
                            <th  class="text-left">Nama</th>
                            <th  class="text-left">Jenis Kelamin</th>
                            <th  class="text-left">Hobi</th>
                            <th  class="text-center">Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($mahasiswa as $item) : ?>
                            <tr>
                                <td class="text-center"><?= $no ?></td>
                                <td class="text-left">
                                    <?= $item['nim'] ?>
                                </td>
                                <td class="text-left">
                                    <?= $item['nama'] ?>
                                </td>
                                <td class="text-left">
                                    <?= ($item['jenis_kelamin'] == 'L') ? 'Laki laki' : 'Perempuan' ?>
                                </td>
                                <td class="text-left">
                                    <?php foreach ($item['hobi'] as $itemHobi) : ?>
                                        <span class="float-left badge bg-primary mx-1 my-1"><?= $itemHobi ?></span>
                                    <?php endforeach; ?>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Aksi</button>
                                        <button type="button" class="btn btn-default dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown">
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item" href="<?= base_url() ?>index.php/mahasiswa/index/edit/<?= $item['id'] ?>">Edit</a>
                                            <button class="dropdown-item" type="button" onclick="confirmDelete('<?= $item['id']; ?>')">Hapus</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
       <form action="<?= base_url() ?>index.php/mahasiswa/index/save" method="POST">
       <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Mahasiswa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nim">NIM</label>
                            <input type="number" name="nim" class="form-control" id="nim" placeholder="Masukan NIM">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan nama">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select name="jk" class="form-control select2" style="width: 100%;height: 100%">
                                <option selected="selected" disabled>Pilih</option>
                                <option value="L">Laki Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" style="height: 38px;" class="form-control" rows="3" placeholder="Masukan alamat"></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Hobi</label>
                            <select name="hobi[]" id="hobi" class="select2" multiple="multiple" data-placeholder="Pilih hobi" style="width: 100%;">
                            
                            <?php foreach($hobi as $item) : ?>
                                <option value="<?= $item['id'] ?>"><?= $item['hobi'] ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="submit" onclick="" class="btn btn-primary">Simpan</button>
            </div>
        </div>
       </form>
    </div>
</div>


<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>
<script>

    function confirmDelete(param)
    {
        var x = confirm("Apakah anda yakin ingin menghapus data ini?");
        if (x){
            window.location.href = "<?= base_url() ?>index.php/mahasiswa/index/delete/"+param;
            return true;
        }else{
          
            return false;
        }

    }

    $(function() {
        $("#mahasiswa").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#mahasiswa_wrapper .col-md-6:eq(0)');
        $('.select2').select2()


        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'classic'
        })
    });
</script>