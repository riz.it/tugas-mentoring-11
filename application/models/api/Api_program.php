<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_program extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getProgram($limit='')
    {
        $result = array();
        $expand = '';
        if ($limit!='' || $limit!=null) {
            $expand = 'limit '.$limit;
        }else {
            $expand = '';
        }
        $sql =$this->db->query("SELECT kp.id_kelas_program,kp.nama_program, kp.paket_makanan,kp.pagu,
    kp.url_banner,kp.created_at,
    gk.nama_grup,gk.tanggal_keberangkatan, gk.tahun_keberangkatan,gk.is_estimasi_tanggal,gk.is_active,gk.is_plus_tour,
    MONTH(gk.tanggal_keberangkatan) AS bulan_berangkat,gk.id_grup,gk.syarat_ketentuan,
    gk.tanggal_pulang,kp.uang_muka_idr,gk.durasi_hari,
    kp.uang_muka_usd, pkt.nama_kategori FROM um_kelas_program kp
    left join um_grup_keberangkatan gk on gk.id_grup=kp.fk_grup_keberangkatan
    left join um_kategori_paket pkt on pkt.id_paket=gk.fk_kategori_paket ORDER BY tanggal_keberangkatan ASC ".$expand);
        if ($sql->num_rows()!=0) {
            $i=0;
            foreach ($sql->result() as $k) {
                if ($k->tahun_keberangkatan>=date('Y') || $k->tanggal_keberangkatan>=date('Y-m-d')) {
                    $result[$i]['id_kelas_program'] = $k->id_kelas_program;
                    $result[$i]['nama_program'] = $k->nama_program;
                    $result[$i]['paket_makanan'] = $k->paket_makanan;
                    $result[$i]['pagu'] = $k->pagu;
                    $result[$i]['plus_tour'] = $k->is_plus_tour;
                    $result[$i]['nama_grup'] = $k->nama_grup;
                    if ($k->url_banner!='' || $k->url_banner!=null) {
                        $result[$i]['url_banner'] = base_url().$k->url_banner;
                    }else {
                        $result[$i]['url_banner'] = base_url('assets/img/coming-soon.png');
                    }
                    $result[$i]['created_at'] = $k->created_at;
                    $result[$i]['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
                    $result[$i]['is_estimasi_tanggal'] = $k->is_estimasi_tanggal;
                    $result[$i]['tahun_keberangkatan'] = $k->tahun_keberangkatan;
                    $result[$i]['bulan_berangkat'] = $k->bulan_berangkat;
                    $result[$i]['id_grup'] = $k->id_grup;
                    $result[$i]['syarat_ketentuan'] = $k->syarat_ketentuan;
                    $result[$i]['tanggal_pulang'] = $k->tanggal_pulang;
                    $result[$i]['uang_muka_idr'] = $k->uang_muka_idr;
                    $result[$i]['durasi_hari'] = $k->durasi_hari;
                    $result[$i]['uang_muka_usd'] = $k->uang_muka_usd;
                    $result[$i]['nama_kategori'] = $k->nama_kategori;

                    $hotel = $this->db->query("SELECT nama_hotel from um_kelas_program_hotel left join m_hotel on m_hotel.id_hotel=um_kelas_program_hotel.fkid_hotel
                        where um_kelas_program_hotel.fkid_kelas_program=?", array($result[$i]['id_kelas_program']))->row();

                    if (!empty($hotel)) {
                        $result[$i]['hotel'] = $hotel->nama_hotel;
                    }else {
                        $result[$i]['hotel'] = 'Belum diisi';

                    }

                    $kamar = $this->db->query("SELECT jenis_kamar, dewasa, anak, bayi from um_kelas_program_kamar where fk_id_kelas_program=?", array($result[$i]['id_kelas_program']));

                    if (!empty($kamar)) {



                        $dt_kamar = $kamar->result();
                        if (count($dt_kamar)!=0) {
                          $nilai_min = array_column($dt_kamar, 'dewasa');
                          $result[$i]['harga_minimum'] = min($nilai_min);
                          $result[$i]['format_harga_minimum'] = number_format($result[$i]['harga_minimum'],2,',','.');
                          $nilai_min = array_column($dt_kamar, 'dewasa');
                          $result[$i]['harga_minimum'] = $nilai_min[0];
                        }else {
                          $result[$i]['format_harga_minimum'] = 0;
                          $result[$i]['harga_minimum'] = 0;
                        }

                    }else {
                        $result[$i]['format_harga_minimum'] = 'Belum diisi';
                        $result[$i]['harga_minimum'] = 'Belum diisi';
                    }



                    $maskapai = $this->db->query("SELECT nama_maskapai from um_grup_keberangkatan_maskapai left join m_maskapai on m_maskapai.id_maskapai=um_grup_keberangkatan_maskapai.fk_maskapai
                        where um_grup_keberangkatan_maskapai.type='Keberangkatan' and um_grup_keberangkatan_maskapai.fkid_grup_keberangkatan=?", array($result[$i]['id_grup']))->row();

                    $result[$i]['maskapai'] = $maskapai->nama_maskapai;


                    $bandara = $this->db->query("SELECT iata, nama_bandara from um_grup_keberangkatan_rute left join m_bandara on m_bandara.id_bandara=um_grup_keberangkatan_rute.fk_id_bandara
                        where um_grup_keberangkatan_rute.type='Keberangkatan' and um_grup_keberangkatan_rute.fk_grup_keberangkatan=?", array($result[$i]['id_grup']))->row();
                    $result[$i]['bandara'] = $bandara->iata.' - '.$bandara->nama_bandara;

                 $i++;
                }
            }

            return ['status'=>'ok','message'=>'Data found','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'Data not found','data'=>'0'];
        }
    }

    public function getHari()
    {
        $hari = $this->db->query("SELECT DISTINCT durasi_hari from um_grup_keberangkatan ORDER BY durasi_hari ASC");
        if ($hari->num_rows() !=0 ) {
            $i=0;
            foreach ($hari->result() as $k) {
                $result[$i]['durasi_hari'] = $k->durasi_hari;
                $i++;
            }
            return ['status'=>'ok','message'=>'Data found','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'Data not found','data'=>'null'];
        }
    }

    public function getDetail($id)
    {
        $result = $this->db->query("SELECT kp.id_kelas_program,kp.nama_program, kp.paket_makanan,kp.pagu,kp.biaya_sudah_termasuk,kp.biaya_belum_termasuk,kp.termasuk_perlengkapan,
            kp.url_banner,kp.created_at,
            gk.nama_grup,gk.tanggal_keberangkatan, gk.tahun_keberangkatan,gk.is_estimasi_tanggal,gk.is_plus_tour,
            MONTH(gk.tanggal_keberangkatan) AS bulan_berangkat, gk.id_grup,gk.syarat_ketentuan,
            gk.tanggal_pulang,kp.uang_muka_idr,gk.durasi_hari,
            kp.uang_muka_usd, pkt.nama_kategori FROM um_kelas_program kp
            left join um_grup_keberangkatan gk on gk.id_grup=kp.fk_grup_keberangkatan
            left join um_kategori_paket pkt on pkt.id_paket=gk.fk_kategori_paket
            WHERE REPLACE(kp.id_kelas_program,'-','')=?", array(str_replace("-", "", $id)));
        if ($result->num_rows()!=0) {
            $k = $result->row();

            $data['id_kelas_program'] = $k->id_kelas_program;
            $data['nama_program'] = $k->nama_program;
            $data['paket_makanan'] = $k->paket_makanan;
            $data['pagu'] = $k->pagu;
            $data['plus_tour'] = $k->is_plus_tour;
            $data['nama_grup'] = $k->nama_grup;
            if ($k->url_banner!='' || $k->url_banner!=null) {
                $data['url_banner'] = base_url().$k->url_banner;
            }else {
                $data['url_banner'] = base_url('assets/img/coming-soon.png');
            }
            $data['created_at'] = $k->created_at;
            $data['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
            $data['tahun_keberangkatan'] = $k->tahun_keberangkatan;
            $data['is_estimasi_tanggal'] = $k->is_estimasi_tanggal;
            $data['bulan_berangkat'] = $k->bulan_berangkat;
            $data['id_grup'] = $k->id_grup;
            $data['syarat_ketentuan'] = $k->syarat_ketentuan;
            $data['tanggal_pulang'] = $k->tanggal_pulang;
            $data['uang_muka_idr'] = $k->uang_muka_idr;
            $data['durasi_hari'] = $k->durasi_hari;
            $data['uang_muka_usd'] = $k->uang_muka_usd;
            $data['nama_kategori'] = $k->nama_kategori;
            $data['biaya_sudah_termasuk'] = $k->biaya_sudah_termasuk;
            $data['biaya_belum_termasuk'] = $k->biaya_belum_termasuk;

            $hotel = $this->db->query("SELECT m_hotel.nama_hotel, m_hotel.fasilitas, m_hotel.id_hotel as id_hotel, m_hotel.jenis FROM um_kelas_program_hotel left join m_hotel on m_hotel.id_hotel=um_kelas_program_hotel.fkid_hotel where um_kelas_program_hotel.fkid_kelas_program=?", array($data['id_kelas_program']));

            $dt_hotel = array();
            if ($hotel->num_rows()!=0) {
                $i=0;
                foreach ($hotel->result() as $rows) {
                    $dt_hotel[$i]['nama_hotel'] = $rows->nama_hotel;
                    $dt_hotel[$i]['fasilitas'] = $rows->fasilitas;
                    $dt_hotel[$i]['id_hotel'] = $rows->id_hotel;
                    $dt_hotel[$i]['jenis'] = $rows->jenis;
                    $i++;
                }
            }else {
                $dt_hotel=null;
            }
            $data['hotel'] =  $dt_hotel;

            $highlight = $this->db->query("SELECT m_highlight.nama_highlight from um_kelas_program_highlight
                      left join m_highlight on m_highlight.id_highlight=um_kelas_program_highlight.fk_highlight
                      where um_kelas_program_highlight.fk_kelas_program=?", array($data['id_kelas_program']));

            $dt_highlight = array();
            if ($highlight->num_rows()!=0) {
                $j=0;
                foreach ($highlight->result() as $rows) {
                    $dt_highlight[$i]['nama_highlight'] = $rows->nama_highlight;
                    $j++;
                }
            }else {
                $dt_highlight=null;
            }

            $data['highlight'] = $dt_highlight;


            $maskapai = $this->db->query("SELECT m_maskapai.nama_maskapai,m_maskapai.fasilitas, um_grup_keberangkatan_maskapai.type
                from um_grup_keberangkatan_maskapai
              left join m_maskapai on m_maskapai.id_maskapai=um_grup_keberangkatan_maskapai.fk_maskapai
              where um_grup_keberangkatan_maskapai.fkid_grup_keberangkatan=?", array($data['id_grup']));

            $dt_maskapai = array();
            if ($maskapai->num_rows()!=0) {
                $k=0;
                foreach ($maskapai->result() as $rows) {
                    $dt_maskapai[$k]['nama_maskapai'] = $rows->nama_maskapai;
                    $dt_maskapai[$k]['fasilitas'] = $rows->fasilitas;
                    $dt_maskapai[$k]['type'] = $rows->type;
                    $k++;
                }
            }else {
                $dt_maskapai=null;
            }

            $data['maskapai'] = $dt_maskapai;




            $dt_harga = array();
            if ($data['is_estimasi_tanggal']=='1') {
                $dt_harga[0]['jenis_kamar'] = "Quad";
                $dt_harga[0]['dewasa'] = "0";
                $dt_harga[0]['anak'] = "0";
                $dt_harga[0]['bayi'] = "0";
                $dt_harga[0]['dt_dewasa'] = 0;
                $dt_harga[0]['dt_anak'] = 0;
                $dt_harga[0]['dt_bayi'] = 0;
            } else {
                $harga = $this->db->query("SELECT jenis_kamar, dewasa, anak, bayi from um_kelas_program_kamar where fk_id_kelas_program=? order by dewasa asc", array($data['id_kelas_program']));

                if ($harga->num_rows()!=0) {
                    $l=0;
                    foreach ($harga->result() as $rows) {
                        $dt_harga[$l]['jenis_kamar'] = $rows->jenis_kamar;
                        $dt_harga[$l]['dewasa'] = $rows->dewasa;
                        $dt_harga[$l]['anak'] = $rows->anak;
                        $dt_harga[$l]['bayi'] = $rows->bayi;
                        $dt_harga[$l]['dt_dewasa'] = number_format($rows->dewasa,2,',','.');
                        $dt_harga[$l]['dt_anak'] = number_format($rows->anak,2,',','.');
                        $dt_harga[$l]['dt_bayi'] = number_format($rows->bayi,2,',','.');
                        $l++;
                    }
                }

            }
            $data['kamar'] = $dt_harga;

            return ['status'=>'ok','message'=>'Data found','data'=>$data];
        }else{
            return ['status'=>'failed','message'=>'Data not found','data'=>'0'];

        }
    }
}
