<?php defined('BASEPATH') or exit('no access allowed');
/**
 * summary
 */
class M_mahasiswa extends MY_Model
{
  /**
   * summary
   */
  protected $_table_name = "mahasiswa";
  protected $_order_by = "id";
  protected $_order_by_type = "ASC";
  protected $_primary_key = "id";

  public function getAllMahasiswa()
  {
    $this->db->select('mahasiswa.*, GROUP_CONCAT(ref_hobi.hobi) AS hobi');
    $this->db->from('mahasiswa');
    $this->db->join('mahasiswa_hobi', 'mahasiswa_hobi.id_mahasiswa = mahasiswa.id', 'left');
    $this->db->join('ref_hobi', 'ref_hobi.id = mahasiswa_hobi.id_hobi', 'left');
    $this->db->group_by('mahasiswa.id');
    $query = $this->db->get();
    $result = $query->result_array();

    foreach ($result as &$row) {
      $row['hobi'] = explode(',', $row['hobi']);
    }

    return $result;
  }

  public function storeMahasiswa($data)
  {
    $this->db->trans_start();

    try {
      $this->db->insert('mahasiswa', [
        'nim' => $data['nim'],
        'nama' => $data['nama'],
        'jenis_kelamin' => $data['jk'],
        'alamat' => $data['alamat']
      ]);

      $id = $this->db->insert_id();
      $hobi = [];
      foreach ($data['hobi'] as $key => $value) {
        $hobi[] = [
          'id_mahasiswa' => $id,
          'id_hobi' => $value
        ];
      }

      $this->db->insert_batch('mahasiswa_hobi', $hobi);

      $this->db->trans_commit();
      $res = [
        'status' => true,
        'message' => 'Data berhasil disimpan.'
      ];
      return $res;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      $res = [
        'status' => false,
        'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while saving data.')
      ];
      return $res;
    }
  }

  public function updateMahasiswa($data, $param)
  {
    $this->db->trans_start();

    try {
      $this->db->where('id', $param);
      $this->db->update('mahasiswa', [
        'nim' => $data['nim'],
        'nama' => $data['nama'],
        'jenis_kelamin' => $data['jk'],
        'alamat' => $data['alamat']
      ]);

      $id = $param;
      $this->db->delete('mahasiswa_hobi', ['id_mahasiswa' => $id]);

      $hobi = [];
      foreach ($this->input->post('hobi') as $key => $value) {
        $hobi[] = [
          'id_mahasiswa' => $id,
          'id_hobi' => $value
        ];
      }

      $this->db->insert_batch('mahasiswa_hobi', $hobi);

      $this->db->trans_commit();
      $res = [
        'status' => true,
        'message' => 'Data berhasil diperbarui.'
      ];
      return $res;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      $res = [
        'status' => false,
        'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while saving data.')
      ];
      return $res;
    }
  }

  public function deleteMahasiswa($id)
  {
    $this->db->trans_start();

    try {
      $this->db->delete('mahasiswa_hobi', ['id_mahasiswa' => $id]);
      $this->db->delete('mahasiswa', ['id' => $id]);
      $this->db->trans_commit();
      $res = [
        'status' => true,
        'message' => 'Data berhasil dihapus.'
      ];
      return $res;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      $res = [
        'status' => false,
        'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while saving data.')
      ];
      return $res;
    }
  }


  public function getMahasiswaById($param)
  {
    $this->db->select('mahasiswa.*, GROUP_CONCAT(ref_hobi.id) AS hobi');
    $this->db->from('mahasiswa');
    $this->db->join('mahasiswa_hobi', 'mahasiswa_hobi.id_mahasiswa = mahasiswa.id', 'left');
    $this->db->join('ref_hobi', 'ref_hobi.id = mahasiswa_hobi.id_hobi', 'left');
    $this->db->group_by('mahasiswa.id');
    $this->db->where('mahasiswa.id', $param);
    $query = $this->db->get();
    $result = $query->row_array();
    $result['hobi'] = explode(',', $result['hobi']);

    return $result;
  }

}
