<?php defined('BASEPATH')OR exit('no access allowed');
/**
  * summary
  */
 class M_hobi extends MY_Model
 {
     /**
      * summary
      */
    protected $_table_name = "ref_hobi";
    protected $_order_by ="id";
    protected $_order_by_type ="ASC";
    protected $_primary_key = "id";


    public function getAllHobi()
    {
        $this->db->select('*');
        $this->db->from('ref_hobi');
        $query = $this->db->get();
        return $query->result_array();
    }
 }
?>